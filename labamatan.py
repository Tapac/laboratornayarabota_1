import math as m
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
print('Доброго времени суток! Пожалуйста, введите число точек разбиения: ')
N = int(input())
print('Отлично! А теперь введите код, отвечающий за способ выбора оснащения (0 - левые точки, 1 - средние и 2 - правые): ')
v = int(input())
x = np.linspace(0,m.pi,10000)
y = [m.cos(i) for i in x]
x1 = np.linspace(0,m.pi,2*N+1)
y1 = [m.cos(i) for i in x1]
fig, ax = plt.subplots()
ax.plot(x,y,linewidth = 2, color = 'red')
ax.grid()
plt.xlabel("x")
plt.ylabel("y")
i=0
summ = 0;
while i<2*N:
    ax.add_patch(patches.Rectangle((x1[i], 0),x1[2],y1[i+v],edgecolor = 'black',facecolor = 'blue',linewidth = 2,fill=True))
    ax.scatter(x1[i+v],y1[i+v],color = 'red')
    summ += x1[2]*y1[i+v]
    i+=2
print(summ)
plt.show()


